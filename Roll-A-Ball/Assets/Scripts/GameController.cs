using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public float timerDuration = 10f;
    public float currentTimer = -1f;
    public bool timerRunning;


    // Start is called before the first frame update
    void Start()
    {
        StartTimer();
    }
    public void StartTimer()
    {
        currentTimer = timerDuration;
        timerRunning = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (timerRunning)
        {
            currentTimer -= Time.deltaTime;

            if (currentTimer <= 0)
            {
                // Timer is Done!
                timerRunning = false;
                // Do whatever

              
            }
        }
    }
}
